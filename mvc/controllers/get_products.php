<?php
require_once "include.php";

class jsonproduct
{
    public $id;
    public $quantity;
    public $maxQuantity;
    public $cost;
    function __construct($id, $quantity, $maxQuantity, $cost) {
        $this->id = $id;
        $this->quantity = $quantity;
        $this->maxQuantity = $maxQuantity;
        $this->cost = $cost;
    }
}

$a = array();
if($_SESSION["userhelper"]->isUserLoggedIn()) {
    $db = $_SESSION["database"];
    $db->setSelectStatement("prodotti_carrello", array("idprodotto", "quantità"),"WHERE idcliente = ?");
    $db->bindParams("i", $_SESSION["userid"]);
    $db->execute();
    $cart = $db->getResult();
    foreach($cart as $prod)
    {
        $maxq = Product::getProductQuantity($prod["idprodotto"]);
        $cost = $db->getParameter("prodotti", $prod["idprodotto"], "idprodotto", "prezzounitario");
        array_push($a, new jsonproduct($prod["idprodotto"], $prod["quantità"], $maxq, $cost));
    }
}

$json = json_encode($a);
// error_log($json);
echo $json;