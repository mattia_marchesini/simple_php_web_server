<?php
require_once("include.php");
$_SESSION["categories"] = Category::getCategoriesList();
if (!$_SESSION["userhelper"]->isUserLoggedIn()) {
    header("Location: ../views/login_form.php");
    die();
}
