<?php
require_once "include.php";

$JSONObjToFunctions =
    [
        "deleteNotification", "updateProduct", "deleteProduct", "checkout", "updateProductSeller", "deleteProductSeller"
    ];

$data = json_decode(file_get_contents("php://input"), true);
foreach (array_keys($data) as $prop) {
    if (in_array($prop, $JSONObjToFunctions)) {
        $prop();
    }
}

function deleteNotification()
{
    global $data;
    $cond;
    $table;
    if ($_SESSION["userhelper"]->isASeller()) {
        $cond = "idvenditore";
        $table = "notifiche_venditori";
    } else {
        $cond = "idcliente";
        $table = "notifiche_clienti";
    }
    $cond = $cond . " = ? AND data = ?";
    $_SESSION["database"]->deleteRow($table, $cond, array($_SESSION["userid"], $data["deleteNotification"]));
}

function deleteProduct()
{
    global $data;
    $db = $_SESSION["database"];
    $prod = $data["deleteProduct"];
    $table = "prodotti_carrello";
    $db->deleteRow($table, "idprodotto = ? AND idcliente = ?", array($prod["id"], $_SESSION["userid"]));
}

function updateProduct()
{
    global $data;
    $db = $_SESSION["database"];
    $prod = $data["updateProduct"];
    $table = "prodotti_carrello";
    $db->setSelectStatement($table, array("idprodotto", "idcliente"), "WHERE idcliente = ? AND idprodotto = ?");
    $db->bindParams("ii", $_SESSION["userid"], $prod["id"]);
    $db->execute();
    if (empty($db->getResult())) {
        $db->setInsertStatement($table, array("idprodotto", "idcliente", "quantità"));
        // error_log($prod["id"], $prod["quantity"]);
        $db->bindParams("iis", $prod["id"], $_SESSION["userid"], $prod["quantity"]);
        $db->execute();
    } else {
        $db->update(
            $table,
            "quantità = ?",
            "iii",
            array($prod["quantity"], $_SESSION["userid"], $prod["id"]),
            "WHERE idcliente = ? AND idprodotto = ?"
        );
    }
}

function checkout()
{
    //prendi prodotti dal carello
    $db = $_SESSION["database"];
    $db->setSelectStatement("prodotti_carrello", array("idprodotto", "quantità"), "WHERE idcliente = ?");
    $db->bindParams("s", $_SESSION["userid"]);
    $db->execute();
    $products = $db->getResult();
    // error_log(print_r($products, true));

    //rimuovi dal carrello
    $db->deleteRow("prodotti_carrello", "idcliente = ?", array($_SESSION["userid"]));

    //inserisci negli ordini
    $db->setInsertStatement("ordini", array("città", "idcliente", "indirizzo"));
    $db->bindParams("sss", Customer::getLocation(), $_SESSION["userid"], Customer::getAddress());
    $db->execute();

    //prendi id ordine
    $db->setSelectStatement("ordini", array("idordine"), "WHERE idcliente = ? ORDER BY data DESC");
    $db->bindParams("s", $_SESSION["userid"]);
    $db->execute();
    $orderID = $db->getResult()[0]["idordine"];

    foreach ($products as $prod) {
        //$_SESSION["debug"] = $orderID;
        //inserisci i singoli prodotti in prodotti_ordine
        Product::insertOrderProduct($prod["idprodotto"], $prod["quantità"], $orderID);
        // $db->setInsertStatement("prodotti_ordine", array("idordine", "idprodotto", "quantità"));
        // $db->bindParams("ssi", $orderID, $prod["idprodotto"], $prod["quantità"]);
        // $db->execute();

        //togli i singoli prodotti dalla tabella prodotti
        //error_log($prod["quantità"]);
        $quantity = Product::getProductQuantity($prod["idprodotto"]) - $prod["quantità"];
        if ($quantity != 0) {
            $db->update("prodotti", "quantità = ?", "is", array($quantity, $prod["idprodotto"]), "WHERE idprodotto = ?");
        } else {
            $db->deleteRow("prodotti", "idprodotto = ?", array($prod["idprodotto"]));
        }
    }

    //notifiche clienti
    $db->setInsertStatement("notifiche_clienti", array("descrizione", "idcliente"));
    $db->bindParams("ss", "l'ordine è partito!", $_SESSION["userid"]);
    $db->execute();

    //notifiche venditori
    $db->setInsertStatement("notifiche_venditori", array("descrizione", "idvenditore"));
    $db->bindParams("ss", "hai ricevuto un ordine!", $_SESSION["userid"]);
    $db->execute();
}

function updateProductSeller()
{
    global $data;
    $db = $_SESSION["database"];
    $table = "prodotti";
    $prod = $data["updateProductSeller"];
    // error_log(print_r($prod, true));
    $db->update(
        $table,
        "quantità = ?",
        "is",
        array($prod["quantity"], $prod["id"]),
        "WHERE idprodotto = ?"
    );
}

function deleteProductSeller()
{
    global $data;
    $db = $_SESSION["database"];
    $table = "prodotti";
    $prod = $data["deleteProductSeller"];
    $db->deleteRow($table, "idprodotto = ?", array($prod["id"]));
}
