<?php
require_once "include.php";
$_POST['productname'] = "Noodle Zen";
$_POST['quantity'] = 1;
if (isset($_POST["remove"])) {
    CartProduct::removeProduct($_POST["remove"]);
}
if ($_SESSION["userhelper"]->isUserLoggedIn()) {
    $_SESSION["cart"] = CartProduct::retrieveCartByUser($_SESSION["userid"]);
    header("Location: ../views/shopping-cart.php");
    die();
} else {
    header("Location: ../views/login_form.php");
    die();
}
