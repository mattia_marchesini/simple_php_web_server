<?php

require_once "include.php";

class jsonproduct
{
    public $id;
    public $quantity;
    function __construct($id, $quantity) {
        $this->id = $id;
        $this->quantity = $quantity;
    }
}

$a = array();
if($_SESSION["userhelper"]->isUserLoggedIn()) {
    $db = $_SESSION["database"];
    $db->setSelectStatement("prodotti", array("idprodotto", "quantità"),"WHERE idvenditore = ?");
    $db->bindParams("i", $_SESSION["userid"]);
    $db->execute();
    $products = $db->getResult();
    foreach($products as $prod)
    {
        // $maxq = Product::getProductQuantity($prod["idprodotto"]);
        // $cost = $db->getParameter("prodotti", $prod["idprodotto"], "idprodotto", "prezzounitario");
        array_push($a, new jsonproduct($prod["idprodotto"], $prod["quantità"]));
    }
}

$json = json_encode($a);
// error_log($json);
echo $json;