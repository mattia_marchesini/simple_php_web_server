<?php
require_once("../controllers/include.php");
if ($_SESSION["userhelper"]->isUserLoggedIn() && $_SESSION["userhelper"]->isASeller()) {
    Seller::getMyProducts();
    // var_dump($_SESSION["userhelper"]->isASeller());
    // var_dump($_SESSION["myproducts"]);
    header("Location: ../views/my-products.php");
    die();
} else {
    header("Location: ../views/catalog.php");
    die();
}
