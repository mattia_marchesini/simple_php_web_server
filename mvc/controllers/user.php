<?php
require_once("../controllers/include.php");
if ($_SESSION["userhelper"]->isUserLoggedIn()) {
    if ($_SESSION["userhelper"]->isASeller()) {
        Seller::getMyOrders();
    } else {
        Customer::getMyOrders();
        // var_dump($_SESSION["myorders"]);
    }
    header("Location: ../views/user.php");
    die();
} else {
    header("Location: ../views/catalog.php");
    die();
}
