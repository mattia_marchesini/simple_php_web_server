<?php
$_SESSION["notifications"] = array();
if (isset($_SESSION["userid"])) {
    if ($_SESSION["userhelper"]->isASeller()) {
        $_SESSION["notifications"] = Seller::getNotifications($_SESSION["userid"]);
        $_SESSION["database"]->update(
            "notifiche_venditori",
            "letta = ?",
            "isi",
            array(1, $_SESSION["userid"], 0),
            "WHERE idvenditore = ? AND letta = ?"
        );
    } else {
        $_SESSION["notifications"] = Customer::getNotifications($_SESSION["userid"]);
        // $_SESSION["database"]->update(
        //     "notifiche_clienti",
        //     "letta = ?",
        //     "isi",
        //     array(1, $_SESSION["userid"], 0),
        //     "WHERE idcliente = ? AND letta = ?"
        // );
        $db = $_SESSION["database"];
        $query = "UPDATE notifiche_clienti SET letta = ? WHERE idcliente = ? AND letta = ?";
        $stmt = $db->prepareQuery($query);
        $l1 = 1; $l0  = 0;
        $stmt->bind_param("isi", $l1, $_SESSION["userid"], $l0);
        $stmt->execute();
    }

    $_SESSION["notificationsPage"] = true;
} else {
    header("Location: ../views/login_form.php");
    die();
}
