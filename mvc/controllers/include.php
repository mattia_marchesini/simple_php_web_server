<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once "../models/Database.php";
require_once "../models/UserHelper.php";
require_once "../models/Product.php";
require_once "../models/CartProduct.php";
require_once "../models/Seller.php";
require_once "../models/Customer.php";
require_once "../models/Category.php";

require_once "../views/View.php";

$_SESSION["database"] = new Database();
$_SESSION["database"]::connectDB();
$_SESSION["userhelper"] = new UserHelper();
$_SESSION["product"] = "";
$_SESSION["has_new_notifications"] = false;
$_SESSION["notificationsPage"] = false;
