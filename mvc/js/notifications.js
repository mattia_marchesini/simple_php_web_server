import * as ajax from "./ajax.js";

function deleteNotification(date) {
    var div = document.getElementById(date);
    div.parentNode.removeChild(div);
    date = date.replace("%", " ");
    ajax.sendJSONToServer({deleteNotification: date});
}

export {deleteNotification}