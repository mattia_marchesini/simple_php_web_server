$(document).ready(function() {
    if ($("#logged-user").length) {
        $("#login-nav")
            .text("Logout")
            .attr("href", "../controllers/logout.php");
    } else {
        $("#login-nav")
            .text("Login")
            .attr("href", "../views/login_form.php");
    }
});