$(document).ready(function() {
    if ($("#cart-table tbody").children().length == 0) {
        $("#cart-table").hide();
        $("#order-summary").hide();
        $("div span #empty-cart")
            .text("Il tuo carrello è vuoto.")
            .addClass("font-weight-light");
        $("div span").addClass("text-center");
    }
});