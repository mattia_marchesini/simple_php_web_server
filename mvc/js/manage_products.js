var products = new Map();
window.products = products;
// $(document).ready(getProductsFromDatabase);
getProductsFromDatabase();

function getProductsFromDatabase() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var productsArray = JSON.parse(this.responseText);
            productsArray.forEach((elem) => {
                products.set(
                    elem.id, 
                    {quantity: elem.quantity}
                );
            });
        }
    };
    xmlhttp.open("GET", "../controllers/manage_products.php", true);
    xmlhttp.send();
}

function addMore(IDProduct) {
    var quantity = parseInt(products.get(IDProduct).quantity) + 1;
    products.get(IDProduct).quantity = quantity.toString();
    updateGraphicalElements(IDProduct, quantity);
    sendProduct(IDProduct, quantity);
}

function addLess(IDProduct) {
    var quantity = parseInt(products.get(IDProduct).quantity) - 1;
    if (quantity == 0) {
        products.delete(IDProduct);
    } else {
        products.get(IDProduct).quantity = quantity.toString();
    }
    updateGraphicalElements(IDProduct, quantity);
    sendProduct(IDProduct, quantity);
}

function deleteProduct(IDProduct) {
    products.delete(IDProduct);
    updateGraphicalElements(IDProduct, 0);
    sendProduct(IDProduct, 0);
}

function updateGraphicalElements(IDProduct, newQuantity) {
    if(newQuantity == 0) {
        var tr = document.getElementById("tr-" + IDProduct);
        tr.parentNode.removeChild(tr);
    }
    else {
        var numProd = document.getElementById("num-prod-" + IDProduct);
        numProd.innerHTML = newQuantity.toString();
    }
}

function sendProduct(IDProduct, quantity) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "../controllers/update_database.php", true); 
    xhttp.setRequestHeader("Content-Type", "application/json");
    // xhttp.onreadystatechange = function() {
    //     if (this.readyState == 4 && this.status == 200) { // Response
    //         optionalFunction();
    //     }
    // };

    if(quantity == 0) {
        xhttp.send(JSON.stringify({deleteProductSeller: {id: IDProduct}}));
    }
    else {
        xhttp.send(JSON.stringify({updateProductSeller: {id: IDProduct, quantity: quantity}}));
    }
}

// function checkout() {
//     var xhttp = new XMLHttpRequest();
//     xhttp.open("POST", "../controllers/update_database.php", true); 
//     xhttp.setRequestHeader("Content-Type", "application/json");
//     xhttp.send(JSON.stringify({checkout: {}}));
//     return true;
// }
