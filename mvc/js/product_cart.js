var products = new Map();
window.products = products;
// $(document).ready(getProductsFromDatabase);
getProductsFromDatabase();

function getProductsFromDatabase() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var productsArray = JSON.parse(this.responseText);
            productsArray.forEach((elem) => {
                products.set(
                    elem.id, 
                    {quantity: elem.quantity, maxQuantity: elem.maxQuantity, cost: elem.cost}
                );

                var buttons = document.getElementsByClassName("btn-prod-" + elem.id);
                Array.prototype.forEach.call(buttons, function(btn) {
                    btn.innerHTML = "elimina";
                    btn.setAttribute('onclick',"deleteProduct(" + elem.id + ")");
                });

                if(parseInt(elem.maxQuantity) == parseInt(elem.quantity)) {
                    document.getElementsById("btn-plus-" + elem.id).disabled = true;
                }
            });
        }
    };
    xmlhttp.open("GET", "../controllers/get_products.php", true);
    xmlhttp.send();
}

function addMore(IDProduct) {
    var quantity = parseInt(products.get(IDProduct).quantity) + 1;
    products.get(IDProduct).quantity = quantity.toString();
    updateGraphicalElements(IDProduct, quantity);
    sendProduct(IDProduct, quantity);
}

function addLess(IDProduct) {
    var quantity = parseInt(products.get(IDProduct).quantity) - 1;
    if (quantity == 0) {
        products.delete(IDProduct);
    } else {
        products.get(IDProduct).quantity = quantity.toString();
    }
    updateGraphicalElements(IDProduct, quantity);
    sendProduct(IDProduct, quantity);
}

function updateGraphicalElements(IDProduct, newQuantity) {
    if(newQuantity == 0) {
        var tr = document.getElementById("tr-" + IDProduct);
        tr.parentNode.removeChild(tr);
    }
    else {
        var numProd = document.getElementById("num-prod-" + IDProduct);
        var priceTotal = document.getElementById("price-total-" + IDProduct);
        numProd.innerHTML = newQuantity.toString();
        //console.log(products.get(IDProduct).cost);
        priceTotal.innerHTML = (newQuantity * parseInt(products.get(IDProduct).cost)).toString() + "€";

        var maxQuantity = parseInt(products.get(IDProduct).maxQuantity)
        if(newQuantity == maxQuantity) {
            var button = document.getElementById("btn-plus-" + IDProduct);
            button.disabled = true;
        }
        else if(newQuantity == maxQuantity - 1) {
            var button = document.getElementById("btn-plus-" + IDProduct);
            button.disabled = false;
        }
    }

    var subtotal = 0;
    products.forEach((val, key) => {
        subtotal += parseInt(val.cost) * parseInt(val.quantity);
    });
    document.getElementById("subtotal").innerHTML = subtotal.toString() + "€";
    document.getElementById("total").innerHTML = (subtotal + 5).toString() + "€";
}

function sendProduct(IDProduct, quantity) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "../controllers/update_database.php", true); 
    xhttp.setRequestHeader("Content-Type", "application/json");
    if(quantity == 0) {
        xhttp.send(JSON.stringify({deleteProduct: {id: IDProduct}}));
    }
    else {
        xhttp.send(JSON.stringify({updateProduct: {id: IDProduct, quantity: quantity}}));
    }
}

function checkout() {
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "../controllers/update_database.php", true); 
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify({checkout: {}}));
    return true;
}

function addProduct(IDProduct) {
    products.set(IDProduct, "1");
    buttons = document.getElementsByClassName("btn-prod-" + IDProduct);
    Array.prototype.forEach.call(buttons, function(btn) {
        btn.innerHTML = "elimina";
        btn.setAttribute('onclick',"deleteProduct(" + IDProduct + ")");
    });
    sendProduct(IDProduct, 1);
}

function deleteProduct(IDProduct) {
    products.delete(IDProduct);
    buttons = document.getElementsByClassName("btn-prod-" + IDProduct);
    Array.prototype.forEach.call(buttons, function(btn) {
        btn.innerHTML = "aggiungi";
        btn.setAttribute('onclick',"addProduct(" + IDProduct + ")");
    });
    sendProduct(IDProduct, 0);
}