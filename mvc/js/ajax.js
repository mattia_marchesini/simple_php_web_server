function sendJSONToServer(data, optionalFunction = function() {}) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "../controllers/update_database.php", true); 
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) { // Response
            optionalFunction();
        }
    };
    xhttp.send(JSON.stringify(data));
}

export {sendJSONToServer};