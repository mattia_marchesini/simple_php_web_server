<?php

class Category
{

    private string $category;
    private array $products;

    private function getProductsFromDatabase()
    {
        $_SESSION["database"]->setSelectStatement("prodotti_categorie", array("idprodotto"), "WHERE categoria = ?");
        $_SESSION["database"]->bindParams("s", $this->category);
        $_SESSION["database"]->execute();
        $a = $_SESSION["database"]->getResult();
        foreach ($a as $value) {
            // echo "id:".$value["idprodotto"].", ";
            array_push($this->products, $value["idprodotto"]);
        }
    }

    function __construct(string $category)
    {
        $this->category = $category;
        $this->products = array();
        $this->getProductsFromDatabase();
    }

    function getCategory()
    {
        return $this->category;
    }

    function get6Products()
    {
        return array_slice($this->products, 0, 6);
    }

    function getProducts()
    {
        return $this->products;
    }

    static function getCategoriesList()
    {
        $_SESSION["database"]->setSelectStatement("prodotti_categorie", array("categoria"), null, true);
        $_SESSION["database"]->execute();
        $a = $_SESSION["database"]->getResult();
        $b = array();

        foreach ($a as $value) {
            array_push($b, $value["categoria"]);
        }
        $_SESSION["cazzo"] = "arborescenza";
        return $b;
    }
}
