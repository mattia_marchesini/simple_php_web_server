<?php

require_once("../controllers/include.php");

class UserHelper
{
    private function checkPassword($pass, $confirmpass)
    {
        return $pass === $confirmpass;
    }

    private function checkEmail($email, $seller)
    {
        $db = $_SESSION["database"];
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if ($seller) {
                $query = "SELECT idvenditore FROM venditori WHERE email = ?";
            } else {
                $query = "SELECT idcliente FROM clienti WHERE email = ?";
            }
            $stmt = $db->prepareQuery($query);
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $result = $stmt->get_result();
            $result->fetch_assoc();
            if ($result->num_rows > 0) {
                $_SESSION["err_registration"] = "Email già registrata. Per favore usa un'email diversa.";
                return false;
            } else if ($result->num_rows === 0) {
                return true;
            }
        } else {
            $_SESSION["err_registration"] = "Inserisci un'email valida.";
            return false;
        }
    }

    function isASeller()
    {
        $db = $_SESSION["database"];
        $id = $_SESSION["userid"];
        $query = "SELECT idvenditore FROM venditori WHERE idvenditore = ?";
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows == 0) {
            return false;
        } else {
            return true;
        }
    }

    function logSeller()
    {
        return isset($_POST["checkbox-seller"]);
    }

    function checkFormData()
    {
        $seller = false;
        if (isset($_POST["partitaIVA"])) {
            $seller = true;
        }
        $msg = "";
        foreach ($_POST as $key => $value) {
            if (empty($value)) {
                $_SESSION["err_registration"] = "Inserisci tutte le informazioni.";
                return array(false, $seller);
            }
        }
        if (!($this->checkEmail($_POST["email"], $seller))) {
            return array(false, $seller);
        }
        if (strlen($_POST["password"]) < 8) {
            $_SESSION["err_registration"] = "Inserisci una password di almeno 8 caratteri.";
            return array(false, $seller);
        }
        if ($this->checkPassword($_POST['password'], $_POST['confirmpassword'])) {
            $_SESSION["msg"] = "Registrazione avvenuta con successo!";
            return array(true, $seller);
        } else {
            $_SESSION["err_registration"] = "Le password non corrispondono.";
            return array(false, $seller);
        }
    }

    function isUserLoggedIn()
    {
        return !empty($_SESSION["userid"]);
    }

    function loginUser()
    {
        // Da applicare anche ai fornitori magari tramite una checkbox
        $db = $_SESSION["database"];
        $email = $_POST['email'];
        $pass = $_POST['password'];
        if (!empty($email) && !empty($pass)) {
            if ($this->logSeller()) {
                $query = "SELECT * FROM venditori WHERE email = ?";
            } else {
                $query = "SELECT * FROM clienti WHERE email = ?";
            }
            $stmt = $db->prepareQuery($query);
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $result = $stmt->get_result();
            $user = $result->fetch_all(MYSQLI_ASSOC);
            if ($result->num_rows == 0) {
                $_SESSION["err_login"] = "Email e/o password errata.";
                return false;
            } else {
                $hash = $user[0]["password"];
                if (password_verify($pass, $hash)) {
                    if ($this->logSeller()) {
                        $_SESSION["userid"] = $user[0]["idvenditore"];
                    } else {
                        $_SESSION["userid"] = $user[0]["idcliente"];
                    }
                    return true;
                } else {
                    $_SESSION["err_login"] = "Email e/o password errata.";
                    return false;
                }
            }
        } else {
            $_SESSION["err_login"] = "Inserisci i dati di accesso.";
        }
    }

    function logOut()
    {
        $_SESSION["userid"] = "";
    }

    function checkInsert()
    {
        $productValues = [];
        $productValues["productname"] = $_POST['productname'];
        $productValues["description"] = $_POST['description'];
        $productValues["quantity"] = $_POST['quantity'];
        $productValues["weight"] = $_POST['weight'];
        $db = $_SESSION["database"];
        foreach ($productValues as $key => $value) {
            if (empty($value)) {
                $_SESSION["err_insert"] = "Inserisci tutti i dati";
                return false;
            }
        }
        $_SESSION["err_insert"] = "Prodotto inserito correttamente.";
        return true;
    }
}
