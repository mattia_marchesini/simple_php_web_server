<?php

class Database
{
    // DATABASE SETTINGS
    const DB_HOST = 'localhost';
    const DB_NAME = 'asian_flavours';
    const DB_USER = 'root';
    const DB_PASSWORD = '';
    const DB_PORT = 3306;

    private static $db = null;
    private $stmt = null;

    // STATIC FUNCTIONS ------------------------------------------------------------------------------------------------
    static function connectDb()
    {
        self::$db = new mysqli(self::DB_HOST, self::DB_USER, self::DB_PASSWORD, self::DB_NAME, self::DB_PORT) or
            die("Connect failed: %s\n" . self::$db->error);
    }

    static function disconnectDb()
    {
        self::$db->close();
    }
    //------------------------------------------------------------------------------------------------------------------

    // STATEMENT SETTERS -----------------------------------------------------------------------------------------------
    private function getPlaceholders(array $paramTags)
    {
        $tags = implode(", ", $paramTags);                               // tags placeholders
        $questionMarks = str_repeat('?, ', count($paramTags) - 1) . '?'; // "?" placeholders
        $types = str_repeat("s", count($paramTags));                     // type placeholders
        return array("tags" => $tags, "questMarks" => $questionMarks, "types" => $types);
    }

    function setSelectStatement(string $table, array $paramTags, ?string $condition = null, bool $distinct = false)
    {
        $plhold = $this->getPlaceholders($paramTags);
        $query = "SELECT ";
        if ($distinct) {
            $query = $query . "DISTINCT ";
        }
        $query = $query . $plhold["tags"] . " FROM " . $table;
        if ($condition != null) {
            $query = $query . " " . $condition;
        }
        //error_log($query);
        $this->stmt = self::$db->prepare($query);
    }

    function setInsertStatement(string $table, array $paramTags)
    {
        $plhold = $this->getPlaceholders($paramTags);
        $query = "INSERT INTO " . $table . " (" . $plhold["tags"] . ") VALUES (" . $plhold["questMarks"] . ")";
        // error_log($query);
        $this->stmt = self::$db->prepare($query);
    }
    //------------------------------------------------------------------------------------------------------------------

    // STATEMENT FUNCTIONS ---------------------------------------------------------------------------------------------
    function bindParams(string $paramTypes, ...$params)
    {
        $this->stmt->bind_param($paramTypes, ...$params);
    }

    function execute()
    {
        $this->stmt->execute();
    }

    function getResult()
    {
        $a = $this->stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $this->stmt = null;
        return $a;
    }

    function prepareQuery($query)
    {
        return self::$db->prepare($query);
    }
    //------------------------------------------------------------------------------------------------------------------

    // UTILITY FUNCTIONS -----------------------------------------------------------------------------------------------
    function getParameters(string $table, $id, string $idTag, string $paramNames)
    {
        $query = "SELECT " . $paramNames . " FROM " . $table . " WHERE " . $idTag . " = ?";
        //error_log(print_r($query, true));
        $stm = self::$db->prepare($query);
        $stm->bind_param("s", $id);
        $stm->execute();
        return $stm->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    function getParameter(string $table, $id, string $idTag, string $paramName)
    {
        $pam = $this->getParameters($table, $id, $idTag, $paramName);
        // error_log(print_r($pam, true));
        if (count($pam) != 0) {
            return $pam[0][$paramName];
        }
        else
        {
            return null;
        }
    }

    function deleteRow(string $table, string $condition, array $params)
    {
        $query = "DELETE FROM " . $table . " WHERE " . $condition;
        // error_log($query);
        $stm = self::$db->prepare($query);
        $plhold = $this->getPlaceholders($params)["types"];
        $stm->bind_param($plhold, ...$params);
        $stm->execute();
    }

    function update(string $table, string $setString, string $paramsTypes, array $params, ?string $condition)
    {
        $query = "UPDATE " . $table . " SET " . $setString;
        if($condition != null)
        {
            $query = $query . " " . $condition;
        }
        // error_log($query);
        $stm = self::$db->prepare($query);
        $stm->bind_param($paramsTypes, ...$params);
        $stm->execute();
    }
    //------------------------------------------------------------------------------------------------------------------
}
