<?php

class Seller
{
    const DATABASE_TABLE = "venditori";
    const ID_ENTITY_TAG = "idvenditore";
    const NAME_ENTITY_TAG = "nome";
    const SURNAME_ENTITY_TAG = "cognome";
    const EMAIL_ENTITY_TAG = "email";
    const PASSWORD_ENTITY_TAG = "password";
    const LOCAT_ENTITY_TAG = "città";
    const ADDRESS_ENTITY_TAG = "indirizzo";
    const IVA_ENTITY_TAG = "partitaIVA";
    const CREDCARDS_ENTITY_TAG = "carte_credito";
    private ?string $sellerID = null;

    static function registerNewSeller()
    {
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $email = $_POST['email'];
        $pass = $_POST['password'];
        //encode password
        $password = password_hash($pass, PASSWORD_DEFAULT);
        $location = $_POST['location'];
        $address = $_POST['address'];
        $iva = $_POST["partitaIVA"];

        $_SESSION["database"]->setInsertStatement(
            self::DATABASE_TABLE,
            array(
                self::NAME_ENTITY_TAG, self::SURNAME_ENTITY_TAG, self::EMAIL_ENTITY_TAG, self::PASSWORD_ENTITY_TAG, self::LOCAT_ENTITY_TAG, self::ADDRESS_ENTITY_TAG, self::IVA_ENTITY_TAG
            ),
        );
        $_SESSION["database"]->bindParams("sssssss", $name, $surname, $email, $password, $location, $address, $iva);
        $_SESSION["database"]->execute();
    }

    static function getNewNotificationsCount(string $userid)
    {
        $condition = "WHERE " . self::ID_ENTITY_TAG . " = ? AND letta = ?";
        $_SESSION["database"]->setSelectStatement("notifiche_venditori", array("data", "descrizione"), $condition);
        $_SESSION["database"]->bindParams("si", $userid, 0);
        $_SESSION["database"]->execute();
        return count($_SESSION["database"]->getResult());
    }

    static function getMyOrders()
    {
        $db = $_SESSION["database"];
        $id = $_SESSION["userid"];
        $query = "SELECT
        prodotti.nome,
        prodotti.idprodotto,
        prodotti.prezzounitario,
        prodotti_ordine.quantità,
        ordini.idordine,
        ordini.data 
        from prodotti 
        inner join prodotti_ordine 
        on prodotti.idprodotto=prodotti_ordine.idprodotto
        inner join ordini 
        on prodotti_ordine.idordine=ordini.idordine  
        where prodotti.idvenditore = ?";
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $orders = $result->fetch_all(MYSQLI_ASSOC);
        return $orders;
    }

    static function getMyProducts()
    {
        $db = $_SESSION["database"];
        $id = $_SESSION["userid"];
        $query = "SELECT * FROM prodotti WHERE idvenditore = ?";
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $products = $result->fetch_all(MYSQLI_ASSOC);
        $_SESSION["myproducts"] = $products;
    }

    static function hasNotifications()
    {
        if (isset($_SESSION["userid"])) {
            return count(Seller::getNotifications($_SESSION["userid"])) != 0;
        }
    }

    static function getNotifications($userid)
    {
        return $_SESSION["database"]->getParameters("notifiche_venditori", $userid, self::ID_ENTITY_TAG, "descrizione, data");
    }

    static function retrieveFromDatabase(string $param)
    {
        return $_SESSION["database"]->getParameter(self::DATABASE_TABLE, $_SESSION["userid"], self::ID_ENTITY_TAG, $param);
    }

    static function getID()
    {
        return $_SESSION["userid"];
    }

    static function getName($userid)
    {
        return Seller::retrieveFromDatabase(self::NAME_ENTITY_TAG);
    }

    static function getSurname()
    {
        return Seller::retrieveFromDatabase(self::SURNAME_ENTITY_TAG);
    }

    static function getEmail()
    {
        return Seller::retrieveFromDatabase(self::EMAIL_ENTITY_TAG);
    }

    static function getPassword()
    {
        return Seller::retrieveFromDatabase(self::PASSWORD_ENTITY_TAG);
    }

    static function getLocation()
    {
        return Seller::retrieveFromDatabase(self::LOCAT_ENTITY_TAG);
    }

    static function getAddress()
    {
        return Seller::retrieveFromDatabase(self::ADDRESS_ENTITY_TAG);
    }

    static function getCreditCards()
    {
        return Seller::retrieveFromDatabase(self::CREDCARDS_ENTITY_TAG);
    }

    static function getIVA()
    {
        return Seller::retrieveFromDatabase(self::IVA_ENTITY_TAG);
    }
}
