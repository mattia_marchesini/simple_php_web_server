<?php

class Customer
{
    const DATABASE_TABLE = "clienti";
    const ID_ENTITY_TAG = "idcliente";
    const NAME_ENTITY_TAG = "nome";
    const SURNAME_ENTITY_TAG = "cognome";
    const EMAIL_ENTITY_TAG = "email";
    const PASSWORD_ENTITY_TAG = "password";
    const LOCAT_ENTITY_TAG = "città";
    const ADDRESS_ENTITY_TAG = "indirizzo";
    const CREDCARDS_ENTITY_TAG = "carte_credito";
    private ?string $customerID = null;

    function __construct(string $customerID)
    {
        $this->customerID = $customerID;
    }

    static function registerUser()
    {
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $email = $_POST['email'];
        $pass = $_POST['password'];
        //encode password
        $password = password_hash($pass, PASSWORD_DEFAULT);
        $location = $_POST['location'];
        $address = $_POST['address'];

        $_SESSION["database"]->setInsertStatement(
            self::DATABASE_TABLE,
            array(
                self::NAME_ENTITY_TAG, self::SURNAME_ENTITY_TAG, self::EMAIL_ENTITY_TAG, self::PASSWORD_ENTITY_TAG, self::LOCAT_ENTITY_TAG, self::ADDRESS_ENTITY_TAG
            ),
        );
        $_SESSION["database"]->bindParams("ssssss", $name, $surname, $email, $password, $location, $address);
        $_SESSION["database"]->execute();
    }

    static function getMyOrders()
    {
        $db = $_SESSION["database"];
        $id = $_SESSION["userid"];
        $query = "SELECT
        ordini.idordine,
        ordini.città,
        ordini.data,
        ordini.indirizzo
        from prodotti 
        inner join prodotti_ordine 
        on prodotti.idprodotto=prodotti_ordine.idprodotto 
        inner join ordini
        on prodotti_ordine.idordine = ordini.idordine
        where ordini.idcliente = ?
        group by ordini.idordine
        order by ordini.data desc, ordini.idordine
        ";
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $orders = $result->fetch_all(MYSQLI_ASSOC);
        $_SESSION["myorders"] = $orders;
        return $orders;
    }

    static function getProductsFromOrder($orderid)
    {
        $db = $_SESSION["database"];
        $query = "SELECT
        prodotti.nome,
        prodotti.idprodotto
        from prodotti 
        inner join prodotti_ordine 
        on prodotti.idprodotto=prodotti_ordine.idprodotto 
        where prodotti_ordine.idordine = ?
        ";
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("i", $orderid);
        $stmt->execute();
        $result = $stmt->get_result();
        $products = $result->fetch_all(MYSQLI_ASSOC);
        $_SESSION["orderproducts"] = $products;
        return $products;
    }

    static function hasNotifications(string $userid)
    {
        return count(Customer::getNotifications($userid)) != 0;
    }

    static function getNewNotificationsCount(string $userid)
    {
        $condition = "WHERE " . self::ID_ENTITY_TAG . " = ? AND letta = ?";
        $_SESSION["database"]->setSelectStatement("notifiche_clienti", array("data", "descrizione"), $condition);
        $_SESSION["database"]->bindParams("si", $userid, 0);
        $_SESSION["database"]->execute();
        return count($_SESSION["database"]->getResult());
    }

    static function getNotifications(string $userid)
    {
        $condition = "WHERE " . self::ID_ENTITY_TAG . " = ? ORDER BY data DESC";
        $_SESSION["database"]->setSelectStatement("notifiche_clienti", array("data", "descrizione", "letta"), $condition);
        $_SESSION["database"]->bindParams("s", $userid);
        $_SESSION["database"]->execute();
        return $_SESSION["database"]->getResult();
    }

    function getNotificationsList()
    {
        return self::getNotifications($this->customerID);
    }

    static function retrieveFromDatabase(string $param)
    {
        return $_SESSION["database"]->getParameter(self::DATABASE_TABLE, $_SESSION["userid"], self::ID_ENTITY_TAG, $param);
    }

    function getID()
    {
        return $this->customerID;
    }

    static function getName($userid)
    {
        return Customer::retrieveFromDatabase(self::NAME_ENTITY_TAG);
    }

    static function getSurname()
    {
        return Customer::retrieveFromDatabase(self::SURNAME_ENTITY_TAG);
    }

    static function getEmail()
    {
        return Customer::retrieveFromDatabase(self::EMAIL_ENTITY_TAG);
    }

    static function getPassword()
    {
        return Customer::retrieveFromDatabase(self::PASSWORD_ENTITY_TAG);
    }

    static function getLocation()
    {
        return Customer::retrieveFromDatabase(self::LOCAT_ENTITY_TAG);
    }

    static function getAddress()
    {
        return Customer::retrieveFromDatabase(self::ADDRESS_ENTITY_TAG);
    }

    static function getCreditCards()
    {
        return Customer::retrieveFromDatabase(self::CREDCARDS_ENTITY_TAG);
    }
}
