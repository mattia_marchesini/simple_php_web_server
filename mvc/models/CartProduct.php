<?php

class CartProduct
{

    static function isInTheCart($productid)
    {
        $db = $_SESSION["database"];
        $query = "SELECT idprodotto FROM prodotti_carrello WHERE idprodotto=?";
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("i", $productid);
        $stmt->execute();
        $result = $stmt->get_result();
        $product = $result->fetch_assoc();
        if (empty($product)) {
            return false;
        } else {
            return true;
        }
    }

    static function removeProduct($productid)
    {
        $db = $_SESSION["database"];
        $query = 'DELETE FROM prodotti_carrello WHERE idprodotto = ?';
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("i", $productid);
        $stmt->execute();
    }

    static function getProductQuantity($productid)
    {
        $db = $_SESSION["database"];
        $query = "SELECT quantità from prodotti_carrello where idcliente = ? AND idprodotto = ?";
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("ii", $_SESSION["userid"], $productid);
        $stmt->execute();
        $result = $stmt->get_result();
        $productData = $result->fetch_assoc();
        //$_SESSION["debug"] = $productData;
        if (!empty($productData)) {
            //$_SESSION["debug"] = $productData;
            return $productData;
        }
    }

    static function insertProductToCart()
    {
        $db = $_SESSION["database"];
        // da modificare
        $productname = $_POST["productname"];
        $quantity = $_POST["quantity"];
        $userid = $_SESSION["userid"];
        list($result, $productid) = Product::retrieveByName($productname);
        if ($result) {
            if (CartProduct::isInTheCart($productid)) {
                // allora prendo il product id e lo uso per la nuova istanza di prodotto_carrello
                "UPDATE prodotti SET quantità=quantità+? WHERE nome=?";
                $query = "UPDATE prodotti_carrello SET quantità=quantità+? WHERE idprodotto=? AND idcliente=?";
                $stmt = $db->prepareQuery($query);
                $stmt->bind_param("iii", $quantity, $productid, $userid);
                $stmt->execute();
            } else {
                $query = "INSERT INTO prodotti_carrello (idprodotto, idcliente, quantità) VALUES (?, ?, ?)";
                $stmt = $db->prepareQuery($query);
                $stmt->bind_param("iii", $productid, $userid, $quantity);
                $stmt->execute();
            }
        }
    }

    static function retrieveCartByUser($userid)
    {
        $db = $_SESSION["database"];
        $query = "SELECT 
        prodotti.nome,
        prodotti.idprodotto,
        prodotti.prezzounitario,
        prodotti_carrello.quantità
        FROM prodotti_carrello
        inner join prodotti
        on prodotti.idprodotto = prodotti_carrello.idprodotto
        WHERE idcliente=?";
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("i", $userid);
        $stmt->execute();
        $result = $stmt->get_result();
        $products = $result->fetch_all(MYSQLI_ASSOC);
        if (!empty($products)) {
            return $products;
        }
    }

    static function subTotalPrice($productid)
    {
        $sum = 0;
        $quantity = CartProduct::getProductQuantity($productid);
        $quantity = $quantity["quantità"];
        $cost = Product::getProductCost($productid);
        $cost = $cost["prezzounitario"];
        for ($i = 0; $i < $quantity; $i++) {
            $sum += $cost;
        }
        return $sum;
    }

    static function totalPrice($products)
    {
        $sum = 0;
        if($products != null) {
            foreach ($products as $product) {
                $sum += CartProduct::subTotalPrice($product["idprodotto"]);
            }
        }
        return $sum;
    }
}
