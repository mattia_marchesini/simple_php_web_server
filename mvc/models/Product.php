<?php

class Product
{
    private $idProduct;
    const TABLE = "prodotti";
    const ID_ENTITY_TAG = "idprodotto";
    const NAME_ENTITY_TAG = "nome";
    const DESCRITPTION_ENTITY_TAG = "descrizione";
    const QUANTITY_ENTITY_TAG = "quantità";
    const PRICE_ENTITY_TAG = "prezzounitario";

    function __construct($idProduct)
    {
        $this->idProduct = $idProduct;
    }

    function getID()
    {
        return $this->idProduct;
    }

    private function retrieveFromDatabase(string $param)
    {
        $a = $_SESSION["database"]->getParameter(self::TABLE, $this->idProduct, self::ID_ENTITY_TAG, $param);
        // var_dump($a);
        return $a;
    }

    function getName()
    {
        return $this->retrieveFromDatabase(self::NAME_ENTITY_TAG);
    }

    function getDescription()
    {
        return $this->retrieveFromDatabase(self::DESCRITPTION_ENTITY_TAG);
    }

    function getPrice()
    {
        return $this->retrieveFromDatabase(self::PRICE_ENTITY_TAG);
    }

    static function retrieveByName($productname)
    {
        $db = $_SESSION["database"];
        $query = "SELECT idprodotto FROM prodotti WHERE nome=?";
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("s", $productname);
        $stmt->execute();
        $result = $stmt->get_result();
        $product = $result->fetch_assoc();
        if (empty($product)) {
            return array(false, $product);
        } else {
            return array(true, $product);
        }
    }

    static function getProductName($productid)
    {
        $db = $_SESSION["database"];
        $query = "SELECT prodotti.nome from prodotti inner join prodotti_carrello on prodotti.idprodotto=prodotti_carrello.idprodotto where prodotti_carrello.idcliente = ? AND prodotti_carrello.idprodotto = ?";
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("ii", $_SESSION["userid"], $productid);
        $stmt->execute();
        $result = $stmt->get_result();
        $productData = $result->fetch_assoc();
        if (!empty($productData)) {
            //$_SESSION["debug"] = $productData;
            return $productData;
        }
    }

    static function getProductCost($productid)
    {
        $db = $_SESSION["database"];
        $query = "SELECT prodotti.prezzounitario from prodotti inner join prodotti_carrello on prodotti.idprodotto=prodotti_carrello.idprodotto where prodotti_carrello.idcliente = ? AND prodotti_carrello.idprodotto = ?";
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("ii", $_SESSION["userid"], $productid);
        $stmt->execute();
        $result = $stmt->get_result();
        $productData = $result->fetch_assoc();
        if (!empty($productData)) {
            //$_SESSION["debug"] = $productData;
            return $productData;
        }
    }

    static function getProductQuantity($productid)
    {
        $db = $_SESSION["database"];
        $a = $db->getParameter(self::TABLE, $productid, self::ID_ENTITY_TAG, self::QUANTITY_ENTITY_TAG);
        //error_log($a == null);
        return $a;
    }

    static function insertProductInCategory($nome)
    {
        $db = $_SESSION["database"];
        $categoria = $_POST['category'];
        list($result, $product) = Product::retrieveByName($nome);
        $_SESSION["debug"] = [$result, $product];
        if ($result) {
            $query = "INSERT INTO prodotti_categorie (categoria, idprodotto) VALUES (?, ?)";
            $stmt = $db->prepareQuery($query);
            $stmt->bind_param("si", $categoria, $product["idprodotto"]);
            $stmt->execute();
        }
    }

    static function insertOrderProduct($id, $qnt, $orderid)
    {
        $db = $_SESSION["database"];
        $query = "INSERT INTO prodotti_ordine (idprodotto, quantità, idordine) VALUES (?, ?, ?)";
        $stmt = $db->prepareQuery($query);
        $stmt->bind_param("iii", $id, $qnt, $orderid);
        $stmt->execute();
    }

    static function insertProduct()
    {
        $db = $_SESSION["database"];
        $productname = $_POST['productname'];
        $description = $_POST['description'];
        $quantity = $_POST['quantity'] / 2;
        $price = $_POST["price"];
        $weight = $_POST['weight'];
        $id = $_SESSION["userid"];
        list($present, $idproduct) = Product::retrieveByName($productname);
        if ($present) {
            $query = "UPDATE prodotti SET quantità=quantità+? WHERE nome=?";
            $stmt = $db->prepareQuery($query);
            $stmt->bind_param("is", $quantity, $productname);
            $stmt->execute();
        } else {
            $query = "INSERT INTO prodotti (nome,descrizione,quantità, prezzounitario, pesounitario, idvenditore) VALUES (?, ?, ?, ?, ?, ?)";
            $stmt = $db->prepareQuery($query);
            $stmt->bind_param("ssiiii", $productname, $description, $quantity, $price, $weight, $id);
            $stmt->execute();
            Product::insertProductInCategory($productname);
        }
    }
}
