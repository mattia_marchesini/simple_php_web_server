<html lang="it">

<head>
    <?php require 'components/head.php'; ?>
    <script type="text/javascript" src="../js/cart.js"></script>
    <title>
        Carrello - Asian Flavours
    </title>
</head>

<body class="bg-gray">
    <?php require 'components/navbar.php'; ?>
    <main>
        <div id="page-content">
            <div class="px-4 px-lg-0 mt-5">
                <div class="pb-5">
                    <div class="container">
                        <div class="row">
                            <div id="cart-card" class="col-lg-12 p-5 bg-white shadow-lg  mb-5">
                                <?php
                                $products = $_SESSION["cart"];
                                if (!empty($products)) { ?>
                                    <h3 class="text-center">Il tuo carrello</h3>
                                <?php } ?>
                                <div class="table-responsive border-0 shadow">
                                    <table id="cart-table" class="table table-sm">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="border-0 bg-light">
                                                    <div class="p-2 px-3 text-uppercase">Immagine</div>
                                                </th>
                                                <th scope="col" class="border-0 bg-light">
                                                    <div class="p-2 px-3 text-uppercase">Prodotto</div>
                                                </th>
                                                <th scope="col" class="border-0 bg-light">
                                                    <div class="py-2 text-uppercase">Prezzo</div>
                                                </th>
                                                <th scope="col" class="border-0 bg-light">
                                                    <div class="py-2 text-uppercase">Quantità</div>
                                                </th>
                                                <th scope="col" class="border-0 bg-light">
                                                    <div class="py-2 text-uppercase">Subtotale</div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($products)) {
                                            ?>
                                                <?php foreach ($products as $prodotto) : ?>
                                                    <?php
                                                    $nome = $prodotto["nome"];
                                                    $prezzo = $prodotto["prezzounitario"];
                                                    $quantità = $prodotto["quantità"];
                                                    $id = $prodotto["idprodotto"];
                                                    ?>
                                                    <tr id='tr-<?php echo $id ?>'>
                                                        <td scope="row"><img class="img-fluid img-thumbnail h-5" src="../../resources/img/p<?php echo $prodotto['idprodotto']; ?>.jpg" alt="Ramen" height=100 width=100></img>
                                                        </td>
                                                        <td scope="row">
                                                            <?php echo $nome; ?>
                                                        </td>
                                                        <td id='price-<?php echo $id ?>'>
                                                            <?php echo $prezzo; ?>€
                                                        </td>
                                                        <td>
                                                            <p id='num-prod-<?php echo $id ?>'><?php echo $quantità; ?></p>
                                                            <div class="btn-group bg-light border-0 h-100">
                                                                <button type="button" onclick='addLess(<?php echo $id ?>)' class='btn align-self-end btn-sm btn-outline-secondary'>
                                                                    -
                                                                </button>
                                                                <button type="button" onclick='addMore(<?php echo $id ?>)' class='btn align-self-end btn-sm btn-outline-secondary' id='btn-plus-<?php echo $prodotto['idprodotto'] ?>'>
                                                                    +
                                                                </button>
                                                            </div>
                                                        </td>
                                                        <td id='price-total-<?php echo $id ?>'>
                                                            <?php echo $quantità * $prezzo; ?>€
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row ml-1 d-flex justify-content-center">
                                    <span>
                                        <h4 id="empty-cart" class="fs-3"></h4>
                                    </span>
                                </div>
                                <!-- End -->
                            </div>
                        </div>
                        <?php if (!empty($products)) { ?>
                            <div id="cart-card" class="row py-5 p-4 bg-white shadow-lg shadow-sm">
                                <div class="col">
                                    <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Riassunto
                                        Ordine
                                    </div>
                                    <div class="p-4">
                                        <p class="font-italic mb-4">Costi di spedizione e tasse potrebbero variare tra i vari
                                            prodotti.</p>
                                        <ul class="list-unstyled mb-4">
                                            <li class="d-flex justify-content-between py-3 border-bottom">
                                                <strong class="text-muted">Subtotale </strong>
                                                <strong id="subtotal">
                                                    <?php
                                                    $total = CartProduct::totalPrice(CartProduct::retrieveCartByUser($_SESSION['userid']));
                                                    echo $total . "€";
                                                    ?>
                                                </strong>
                                            </li>
                                            <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Costi di spedizione</strong><strong>5.00€</strong>
                                            </li>
                                            <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">IVA incl.</strong><strong>0.00€</strong></li>
                                            <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Totale</strong>
                                                <h5 id="total" class="font-weight-bold"><?php echo $total + 5 ?>€</h5>
                                            </li>
                                        </ul>
                                        <a onclick="checkout()" href="checkout.php" class="btn btn-dark rounded-pill py-2 btn-block">Checkout</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php
    require "components/foot.php"
    ?>
    <script src="../js/product_cart.js"></script>
</body>

</html>