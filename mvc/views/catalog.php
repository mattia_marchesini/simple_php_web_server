<?php require_once "../controllers/include.php"; ?>
<!DOCTYPE html>
<html lang="it">

<head>
    <?php require "components/head.php"; ?>
    <?php require "../controllers/catalog.php"; ?>
    <title>Asian Flavours - Catalogo prodotti</title>
</head>

<body>

    <?php
    require "components/navbar.php";
    ?>

    <main class="mt-5">
        <h1 class="text-center text-light">Catalogo</h1>
        <?php if (!empty($_SESSION["categories"])) { ?>
            <?php
            foreach ($_SESSION["categories"] as $value) {
                $_SESSION["single_category"] = $value;
                require "components/product_article.php";
            }
            unset($_SESSION["single_category"]);
            unset($_SESSION["categories"]);
            unset($_SESSION["single_category"]);
            ?>
        <?php } else { ?>

        <?php } ?>
    </main>
    <?php
    require "components/foot.php"
    ?>
    <script src="../js/product_cart.js"></script>
</body>

</html>