<html lang="it">

<head>
    <?php require 'components/head.php'; ?>
    <!-- <script type="text/javascript" src="../js/my_products.js"></script> -->
    <title>
        Asian Flavours
    </title>
</head>

<body>
    <?php require 'components/navbar.php'; ?>
    <div id="page-content" class="d-flex card border-bottom-0 justify-content-center">
        <div id="table-title" class="text-center">
            <h4 class="fst-italic text-light font-weight-bold d-flex justify-content-center mb-0 mt-2">I tuoi prodotti</h4>
        </div>
        <div id="table" class="card-body d-inline-flex justify-content-center mt-0">
            <div class="table-responsive mx-auto text-center  bg-light  mx-2 w-75 shadow">
                <table id="my-products-table" class="table table-sm border-0 table-hover">
                    <thead class="border-0">
                        <tr>
                            <th scope="col">
                                Immagine
                            </th>
                            <th scope="col">
                                Nome
                            </th>
                            <th scope="col">
                                Descrizione
                            </th>
                            <th scope="col">
                                Quantità
                            </th>
                            <th scope="col">
                                Prezzo Unitario
                            </th>
                            <th scope="col">
                                Peso Unitario
                            </th>
                            <th scope="col">
                                Elimina
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        Seller::getMyProducts();
                        $products = $_SESSION["myproducts"];
                        ?>
                        <?php foreach ($products as $prodotto) : ?>
                            <?php
                            $nome = $prodotto["nome"];
                            $descrizione = $prodotto["descrizione"];
                            $quantità = $prodotto["quantità"];
                            $id = $prodotto["idprodotto"];
                            ?>
                            <tr id='tr-<?php echo $id; ?>'>
                                <td scope="row"><img class="img-fluid img-thumbnail h-5" src="../../resources/img/p<?php echo $id; ?>.jpg" alt="Ramen" height=100 width=100></img>
                                </td>
                                <td scope="row">
                                    <?php echo $nome; ?>
                                </td>
                                <td>
                                    <?php echo $descrizione; ?>
                                </td>
                                <td>
                                    <p id='num-prod-<?php echo $id ?>'><?php echo $quantità; ?></p>
                                    <div class="btn-group bg-light border-0 h-100">
                                        <button type="button" onclick="addLess(<?php echo $id ?>)" class="btn align-self-end btn-sm btn-outline-secondary disabled-btn">
                                            -
                                        </button>
                                        <button type="button" onclick="addMore(<?php echo $id ?>)" class="btn align-self-end btn-sm btn-outline-secondary">
                                            +
                                        </button>
                                    </div>
                                </td>
                                <td> <?php echo $prodotto["prezzounitario"]; ?> </td>
                                <td> <?php echo $prodotto["pesounitario"]; ?> </td>
                                <td><button onclick='deleteProduct(<?php echo $id; ?>)' role="button" class="btn"><i class="fas fa-trash"></i></button></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <div id="empty">
                <span class="d-flex px-2 justify-content-center">
                    <h5 id="empty-myproducts"></h5>
                </span>
            </div>
        </div>
    </div>
    <div class='d-flex justify-content-center row'>
        <a class='mx-2 text-dark' href='insert-product.php'>
            <span id="btn" class='card shadow px-3 text-center'>
                <h5>Inserisci Prodotto</h5>
            </span>
        </a>
    </div>
    <?php
    require "components/foot.php"
    ?>
    <script src="../js/footer.js"></script>
    <script src="../js/manage_products.js"></script>
</body>

</html>