<!DOCTYPE html>
<html lang="it">
<head>
  <?php require 'head.php'; ?>
  <title>Asian Flavours - Zona Utente</title>
</head>
<body>
    <?php require "navbar.php";?>

    <main role="main" class="container">
      <div class="d-flex align-items-center p-3 my-3 box-shadow">
        <img class="mr-3" src="../../resources/img/user.png" alt="profile picture" width="64px">
        <div class="lh-100">
          <p class="mb-0 lh-100"><?php echo "name";?></p>
          <p class="mb-0 lh-100"><?php echo "surname";?></p>
        </div>
      </div>

      <div class="my-3 p-3 bg-white rounded box-shadow">
        <div class="container-fluid">
          <div class="row border-bottom border-gray">
            <div class="col-10 col-md-11 ">
              <h2>Modifica dati</h2>
            </div>
            <div class="col-2 col-md-1 float-right">
              <button aria-label="Edit fields" onclick="modify()"><i class="fa fa-pencil"></i></button>
            </div>
          </div>
        </div>
        <div class="media text-muted pt-3">
          <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="d-flex justify-content-between align-items-center w-100">
              <strong class="text-gray-dark">Nome</strong>
              <!-- <a href="#">modifica</a> -->
            </div>
            <span class="d-block"><?php echo "name";?></span>
          </div>
        </div>
        <div class="media text-muted pt-3">
          <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="d-flex justify-content-between align-items-center w-100">
              <strong class="text-gray-dark">Cognome</strong>
              <a href="#">modifica</a>
            </div>
            <span class="d-block"><?php echo "surname";?></span>
          </div>
        </div>
        <div class="media text-muted pt-3">
          <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="d-flex justify-content-between align-items-center w-100">
              <strong class="text-gray-dark">Indirizzo</strong>
              <a href="#">modifica</a>
            </div>
            <span class="d-block"><?php echo "address";?></span>
          </div>
        </div>
        <div class="media text-muted pt-3 hidden-field">
          <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="d-flex justify-content-between align-items-center w-100">
              <strong class="text-gray-dark">Località</strong>
              <a href="#">modifica</a>
            </div>
            <span class="d-block"><?php echo "location";?></span>
          </div>
        </div>
        <small class="d-block text-right mt-3">
          <a href="#">altri</a>
        </small>
      </div>

      <div class="my-3 p-3 bg-white rounded box-shadow">
        <h2 class="border-bottom border-gray pb-2 mb-0">Ordini precedenti</h2>
        <div class="media text-muted pt-3">
          <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="d-flex justify-content-between align-items-center w-100">
              <strong class="text-gray-dark">data</strong>
              <a href="#">riordina</a>
            </div>
            <span class="d-block">questo, quello, quell'altro...</span>
          </div>
        </div>
        <div class="media text-muted pt-3">
          <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="d-flex justify-content-between align-items-center w-100">
              <strong class="text-gray-dark">Cognome</strong>
              <a href="#">riordina</a>
            </div>
            <span class="d-block">Surname</span>
          </div>
        </div>
        <div class="media text-muted pt-3">
          <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="d-flex justify-content-between align-items-center w-100">
              <strong class="text-gray-dark">Indirizzo</strong>
              <a href="#">riordina</a>
            </div>
            <span class="d-block">Adrress</span>
          </div>
        </div>
        <small class="d-block text-right mt-3">
          <a href="#">altri</a>
        </small>
      </div>
    </main>
</body>
</html>