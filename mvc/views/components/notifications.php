<!DOCTYPE html>
<html lang="it">

<head>
    <?php require "head.php"; ?>
    <title>Asian Flavours - Notifications</title>
</head>

<body>
    <?php require "navbar.php"; ?>

    <main role="main" class="container">
        <div class="my-3 p-3 mt-5 mb-5 h-100 bg-white shadow-lg rounded">
            <h6 class="border-bottom border-gray pb-2 mb-0">Notifiche Recenti</h6>
            <?php

            foreach ($_SESSION["notifications"] as $value) {
                $_SESSION["single_notification"] = $value;
                require "single_notification.php";
            }
            unset($_SESSION["single_notification"]);
            unset($_SESSION["notifications"]);
            ?>
        </div>
    </main>
</body>

</html>