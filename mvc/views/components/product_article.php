<article class="mt-3">
    <div id="article-album" class="album py-3">
        <h2 class="text-center text-light font-weight-bold"><?php echo $_SESSION["single_category"] ?></h2>
        <div id="article-div" class="container-fluid d-flex justify-content-center">
            <div id="article-card" class="row col-10 bg-light shadow-lg d-flex justify-content-center">
                <?php
                $cat = new Category($_SESSION["single_category"]);
                foreach ($cat->get6Products() as $val) {
                    // echo "<div class='col-6 col-sm-3 col-lg-2 my-2'>";
                    $prod = new Product($val);
                    $_SESSION["single_product"] = array(
                        "name" => $prod->getName(),
                        "description" => $prod->getDescription(),
                        "id" => $prod->getID(),
                        "price" => $prod->getPrice()
                    );
                    require "product_card.php";
                    // echo "</div>";
                }
                ?>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        <a class="text-center show-more text-light mb-5 font-weight-bold" href='product_category.php?category=<?php echo str_replace(" ", "%20", $cat->getCategory()); ?>'>
            Vedi altro...
        </a>
    </div>
</article>