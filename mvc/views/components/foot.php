<!-- Footer -->
<div class="h-50"></div>
<div class="container-fluid p-4">
    <footer id="footer" style="background-color: var(--primary)" class="bg-light w-75 mx-auto text-lg-start">
        <!-- Grid container -->
        <!--Grid row-->
        <div class="row d-flex justify-content-center ">
            <div class="col-12 text-center col-lg-12 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase font-weight-bold mb-0">Contatti</h5>
                <ul class="list-unstyled text-center my">
                    <li>
                        <a href="#!" class="text-dark">lorenzo.massone@studio.unibo.it</a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">mattia.marchesini@studio.unibo.it</a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">+39 321456789</a>
                    </li>
                </ul>
            </div>
            <!--Grid column-->
        </div>
        <!--Grid row-->
</div>
<!-- Grid container -->