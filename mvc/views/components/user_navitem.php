<?php
if ($_SESSION["userhelper"]->isASeller()) {
    $username = Seller::getName($_SESSION['userid']);
} else {
    $username = Customer::getName($_SESSION['userid']);
}
?>
<li class="nav-item active dropdown">
    <a id='logged-user' class='nav-link m-0 mt-1 py-0 pb-1  active dropdown-toggle' data-toggle='dropdown' aria-haspopup="true" aria-expanded="false">
        <i class="fa text-dark fa-user"></i> <b class="text-dark"><?php echo $username; ?></b>
    </a>

    <div class="dropdown-menu text-dark shadow" style="background-color: var(--primary)" aria-labelledby="logged-user">
        <a class="dropdown-item" href="user.php">Pagina Utente</a>
        <a class="dropdown-item" href="../controllers/logout.php">Logout</a>
    </div>
</li>