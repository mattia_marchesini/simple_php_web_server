<?php
$notfCount = 0;
$id = $_SESSION["userid"];
if ($_SESSION["userhelper"]->isASeller()) {
    $notfCount = Seller::getNewNotificationsCount($id);
} else {
    $notfCount = Customer::getNewNotificationsCount($id);
}
$cond = (($notfCount > 0) && (!$_SESSION["notificationsPage"]));
?>
<li class="nav-item">
    <a class='nav-link m-0 py-0 <?php if ($cond) {
                                    echo "active";
                                } ?>' href="notifications.php">
        <i aria-label="Notifications" id="notification_bell" class='fa text-dark fa-bell py-0 mr-1'></i>
        <?php
        if ($cond) {
            echo "<span class='badge badge-primary rounded-circle'>" .
                $notfCount .
                "</span>";
        }
        ?>
    </a>
</li>
<li class="nav-item">
    <a href="../controllers/manage-cart.php">
        <i aria-label="Cart" class="text-light pt-1 mr-2 text-dark mb-0 fas fa-shopping-cart"></i>
    </a>
</li>