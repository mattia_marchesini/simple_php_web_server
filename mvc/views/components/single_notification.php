<?php $notf = str_replace(" ", "%", $_SESSION["single_notification"]["data"]) ?>

<div id='<?php echo ($notf) ?>' class="text-muted pt-3 border-bottom border-gray h-auto">
  <p class="text-gray-dark pb-3 mb-0 small">
    <strong>
      <?php
      $data = $_SESSION["single_notification"]["data"];
      $data_giorni = date('d/m/y', strtotime($data));
      $data_ora = date('h:i', strtotime($data));
      echo $data_giorni;
      ?>
    </strong>
    <span>&ensp;</span>
    <strong>
      <?php
      echo $data_ora;
      ?>
    </strong>
  <div class="d-flex flex-wrap">
    <p class="text-gray-dark pb-3 mb-0 small"><?php echo $_SESSION["single_notification"]["descrizione"]; ?></p>
    <button class="ml-auto" onclick="deleteNotification('<?php echo ($notf) ?>')">
      elimina
    </button>
  </div>
</div>