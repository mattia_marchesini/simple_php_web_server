<div class="col-lg-2 col-sm-4 col-12">
    <div class="my-4 d-flex justify-content-center text-center">
        <div class="card d-flex justify-content-center catalog-card shadow-lg border box-shadow">
            <img class="card-img-top border img-thumbnail " style="height: 150px" alt="<?php echo $_SESSION["single_product"]["name"] ?>" src="../../resources/img/p<?php echo $_SESSION["single_product"]["id"] ?>.jpg">
            <h5 class="card-title align-top font-weight-bold text-center">
                <a class="text-dark" href="single-product.php?id=<?php echo $_SESSION["single_product"]["id"] ?>"><?php echo $_SESSION["single_product"]["name"] ?></a>
            </h5>
            <div class="card-footer row mt-3 d-flex justify-content-center border-0">
                <button onclick="addProduct(<?php echo $_SESSION['single_product']['id'] ?>)" class='btn btn-sm btn-outline-dark btn-prod-<?php echo $_SESSION["single_product"]["id"] ?>'>
                    aggiungi
                </button>
            </div>
        </div>
    </div>
</div>