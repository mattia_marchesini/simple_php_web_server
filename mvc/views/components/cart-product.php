<?php
$products = $_SESSION["cart"];
?>
<? foreach ($products as $prodotto) { ?>

<? } ?>


<tr>
    <th scope="row">
        <div class="p-2">
            <img src=<?php echo "../../resources/img/ramen.jpg" ?> alt="" width="70" class="img-fluid rounded shadow-sm">
            <div class="ml-3 d-inline-block align-middle">
                <h5 class="mb-0">
                    <a href="#" class="text-dark d-inline-block align-middle">
                        <!-- metterci nome prodotto -->
                        <?php
                        $productname =  Product::getProductName($prodotto["idprodotto"]);
                        //var_dump($productname);
                        echo $productname["nome"];
                        ?>
                    </a>
                </h5>
            </div>
        </div>
    </th>
    <td id="prezzo" class="border-0 align-middle">
        <strong>
            <?php
            $productcost =  CartProduct::subTotalPrice($prodotto["idprodotto"]);
            //var_dump($productcost);
            echo $productcost;
            ?> €
        </strong>
    </td>
    <td class="border-0 align-middle"><strong>
            <?php $productquantity = CartProduct::getProductQuantity($prodotto['idprodotto']);
            echo $productquantity['quantità']; ?>
        </strong>
    </td>
    <td class="border-0 align-middle">
        <form method="post" action="../controllers/manage-cart.php">
            <button name="remove" value="<?php echo $prodotto['idprodotto']; ?>" class="text-dark">
                <i class="fa fa-trash"></i>
            </button>
        </form>
    </td>
</tr>