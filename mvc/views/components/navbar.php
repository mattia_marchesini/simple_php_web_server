    <div id="navbar-div" class="d-flex justify-content-center rounded ">
        <nav id="navbar" class="navbar navbar-expand-md navbar-dark d-flex align-items-center h-25 w-75">
            <div class="flex-md-column ml-3 mr-5">
                <a class="navbar-brand text-dark fs-5" href="catalog.php">
                    <img src="../../resources/img/brand.png" width="30" height="30" class="d-inline-block align-top" alt="ramen-icon">
                    Asian Flavours
                </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="true" aria-label="Toggle navigation" aria-label="dropdown menu">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse align-middle text-center" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                </ul>
                <ul class="navbar-nav  mr-0">
                    <?php
                    if ($_SESSION["userhelper"]->isUserLoggedIn()) {
                        require "notification_bell.php";
                        require "user_navitem.php";
                    } else {
                        require "no_notification_bell.php";
                        require "login_navitem.php";
                    }
                    ?>
                </ul>
            </div>
        </nav>
    </div>