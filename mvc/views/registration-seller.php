<html lang="it">

<head>
    <?php require 'components/head.php'; ?>
    <script src="../js/script.js"></script>
    <title>
        Asian Flavors
    </title>
</head>

<body class="bg-gray">
    <?php require 'components/navbar.php'; ?>
    <div id="page-content">
        <div class="container d-flex justify-content-center my-0 pb-0 bg-gray">
            <div class="card shadow d-flex justify-content-center mt-5 w-75">
                <div id="error-row" class="row mt-3 justify-content-center">
                    <span class="w-25 card text-center" id="error-span">
                        <small>
                            <?php
                            if (isset($_SESSION["err_registration"])) {
                                echo $_SESSION["err_registration"];
                                $_SESSION["err_registration"] = "";
                            }
                            ?>
                        </small>
                    </span>
                </div>
                <div id="registration-row" class="row mt-2 pt-0 justify-content-center">
                    <div id="registration-col" class="col-6">
                        <div id="registration-box">
                            <form method="post" action="registration.php">
                                <div id="titolo-form" class="text-center pb-2 ">
                                    <h4>Registrazione</h4>
                                </div>
                                <div id="campo-registrazione">
                                    <input type="text" class="form-control" placeholder="Nome" id="nome" name="name">
                                </div>
                                <div id="campo-registrazione">
                                    <input type="text" class="form-control" placeholder="Cognome" id="cognome" name="surname">
                                </div>
                                <div id="campo-registrazione">
                                    <input type="text" class="form-control" placeholder="Città" id="città" name="location">
                                </div>
                                <div id="campo-registrazione">
                                    <input type="text" class="form-control" placeholder="Indirizzo" id="indirizzo" name="address">
                                </div>
                                <div id="campo-registrazione">
                                    <input type="email" class="form-control" placeholder="Email" id="email" name="email">
                                </div>
                                <div id="campo-registrazione">
                                    <input type="password" minlength="8" class="form-control" placeholder="Password" id="password" name="password">
                                </div>
                                <div id="campo-registrazione">
                                    <input type="password" minlength="8" class="form-control" placeholder="Conferma Password" id="confermapassword" name="confirmpassword">
                                </div>
                                <div id="campo-registrazione">
                                    <input type="tel" class="form-control" minlength="11" maxlength="11" min="1" placeholder="Partita IVA" id="partitaIVA" name="partitaIVA" required>
                                </div>
                                <div id="link-cliente" class="text-center text-primary mt-2 mb-4">
                                    <a href="registration-form.php">
                                        Sono un Cliente
                                    </a>
                                </div>
                                <div id="submit-registrazione" class="text-center">
                                    <button id="submit-registration" type="submit" class="btn-sm">Registrati</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            </form>
        </div>
    </div>
    <?php
    require "components/foot.php"
    ?>
</body>

</html>