<?php

class View
{

    private string $page;

    function __construct(string $page)
    {
        $this->page = $page;
    }

    function renderPage()
    {
        require "components/" . $this->page . ".php";
    }
}
