<div class="my-3 mb-5 p-3 bg-white rounded box-shadow shadow">
    <h2 class="border-0 text-center border-gray pb-2 mb-0">
        Ordini Precedenti
    </h2>
    <?php $orders = Customer::getMyOrders(); ?>
    <?php foreach ($orders as $ordine) : ?>
        <?php
        $products = Customer::getProductsFromOrder($ordine["idordine"]);
        ?>
        <div class='d-flex'>
            <a class='text-dark' href='../controllers/my-products.php'>
                <span class='card date-span shadow mb-2 px-2'>
                    <h6>
                        <?php
                        $valid_date = date('d/m/y', strtotime($ordine["data"]));
                        echo $valid_date;
                        ?>
                    </h6>
                </span>
            </a>
        </div>
        <table id="my-products-table" class="table table-bordered shadow table-sm table-hover">
            <thead>
                <tr>
                    <th scope="col">
                        Immagine
                    </th>
                    <th scope="col">
                        Nome
                    </th>
                    <th scope="col">
                        Città
                    </th>
                    <th scope="col">
                        Indirizzo
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product) : ?>
                    <tr>
                        <?php
                        $nome = $product["nome"];
                        $città = $ordine["città"];
                        $indirizzo = $ordine["indirizzo"];
                        ?>
                        <td><img class="img-fluid img-thumbnail h-5" src="../../resources/img/p<?php echo $product["idprodotto"]; ?>.jpg" alt="Ramen" height=100 width=100></img>
                        </td>
                        <td>
                            <?php echo $product["nome"]; ?>
                        </td>
                        <td>
                            <?php echo $città; ?>
                        </td>
                        <td>
                            <?php echo $indirizzo; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endforeach; ?>
</div>