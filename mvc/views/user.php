<!DOCTYPE html>
<html lang="it">

<head>
  <?php
  require "components/head.php";
  ?>
  <title>Asian Flavors - Dati Utente</title>
</head>

<body>
  <?php
  require "components/navbar.php";
  ?>

  <?php
  $orders = $_SESSION["myorders"];
  ?>

  <main role="main" class="container">
    <div class="d-flex row justify-content-center p-3 my-3 box-shadow">
      <div class="lh-100">
      </div>
    </div>

    <?php
    if ($_SESSION["userhelper"]->isASeller()) {
      echo "<div class='d-flex justify-content-center'> <a class='text-dark' href='../controllers/my-products.php'><span id='my-products' class='card shadow px-2 text-center'> <h5>I miei prodotti</h5></span> </a> </div>";
    }
    ?>
    <div class="my-3 p-3 bg-white rounded box-shadow shadow">
      <h1 class="border-bottom border-gray text-center pb-2 mb-0">I tuoi dati</h1>
      <div class="media shadow-sm px-1 text-muted pt-3">
        <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
          <div class="d-flex justify-content-between align-items-center w-100">
            <strong class="text-gray-dark">Nome</strong>
          </div>
          <span class="d-block">
            <?php if ($_SESSION["userhelper"]->isASeller()) {
              echo Seller::getName($_SESSION['userid']);
            } else {
              echo Customer::getName($_SESSION['userid']);
            }
            ?>
          </span>
        </div>
      </div>
      <div class="media shadow-sm px-1 text-muted pt-3">
        <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
          <div class="d-flex justify-content-between align-items-center w-100">
            <strong class="text-gray-dark">Cognome</strong>
          </div>
          <span class="d-block">
            <?php if ($_SESSION["userhelper"]->isASeller()) {
              echo Seller::getSurname();
            } else {
              echo Customer::getSurname();
            }
            ?>
          </span>
        </div>
      </div>
      <div class="media shadow-sm px-1 text-muted pt-3">
        <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
          <div class="d-flex justify-content-between align-items-center w-100">
            <strong class="text-gray-dark">Città</strong>
          </div>
          <span class="d-block">
            <?php if ($_SESSION["userhelper"]->isASeller()) {
              echo Seller::getLocation();
            } else {
              echo Customer::getLocation();
            }
            ?>
          </span>
        </div>
      </div>
      <div class="media shadow-sm px-1 text-muted pt-3">
        <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
          <div class="d-flex justify-content-between align-items-center w-100">
            <strong class="text-gray-dark">Indirizzo</strong>
          </div>
          <span class="d-block">
            <?php if ($_SESSION["userhelper"]->isASeller()) {
              echo Seller::getAddress();
            } else {
              echo Customer::getAddress();
            }
            ?>
          </span>
        </div>
      </div>
      <div class="media text-muted pt-3" style="visibility: hidden;">
        <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
          <span class="d-block"></span>
        </div>
      </div>
    </div>

    <?php
    if ($_SESSION["userhelper"]->isASeller()) {
      require "orders-seller.php";
    } else {
      require "orders-customer.php";
    }
    ?>

  </main>
  <?php
  require "components/foot.php"
  ?>
</body>

</html>