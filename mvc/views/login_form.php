<html lang="it">

<head>
    <?php require 'components/head.php'; ?>
    <script src="../js/ajax.js"></script>
    <title>Asian Flavors - Login</title>
</head>


<body class="bg-gray">
    <?php require 'components/navbar.php'; ?>
    <div id="page-content">
        <div class="container d-flex my-0 justify-content-center" id="login">
            <div class="card shadow d-flex px-0 justify-content-center mt-5 w-75">
                <div id="error-row" class="row mt-3 justify-content-center">
                    <span class="w-25 card text-center" id="error-span">
                        <small>
                            <?php
                            if (isset($_SESSION["err_login"])) {
                                echo $_SESSION["err_login"];
                                $_SESSION["err_login"] = "";
                            }
                            if (isset($_SESSION["msg"])) {
                                echo $_SESSION["msg"];
                                $_SESSION["msg"] = "";
                            }
                            ?>
                        </small>
                    </span>
                </div>
                <div id="login-row" class="row d-flex pt-1 justify-content-center w-80">
                    <div id="login-column" class="col-6">
                        <div id="login-box" class="col">
                            <form id="login-form" class="form" action="../controllers/login.php" method="post">
                                <h4 id="titolo-form" class="text-center text-black pt-2">Login</h4>
                                <div class="form-group">
                                    <label for="email" class="text-black">Email:</label><br>
                                    <input type="email" class="form-control" id="email" name="email" required>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="text-black">Password:</label><br>
                                    <input type="password" name="password" id="password" class="form-control" required>
                                </div>
                        </div>
                        <div id="submit-row">
                            <div class="form-check d-flex justify-content-between mx-0">
                                <div class="col-6">
                                    <input class="form-check-input" type="checkbox" name="checkbox-seller" value="seller" id="checkbox-seller">
                                    <label class="form-check-label" for="checkbox-seller">
                                        Sono un Venditore
                                    </label>
                                </div>
                                <div id="register" class="col-6 text-right">
                                    <a href="registration-form.php">
                                        Non sei registrato?
                                    </a>
                                </div>
                            </div>
                            <div class="form-group text-center" id="submit-registrazione" class="text-center">
                                <button id="submit" type="submit" class="btn-sm">Accedi</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    require "components/foot.php"
    ?>
</body>

</html>