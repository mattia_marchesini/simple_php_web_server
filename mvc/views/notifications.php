<?php require_once "../controllers/include.php"; ?>
<?php require "../controllers/notifications.php"; ?>
<!DOCTYPE html>
<html lang="it">

<head>
    <?php require "components/head.php"; ?>
    <title>Asian Flavours - Notifications</title>
</head>

<body>
    <?php require "components/navbar.php"; ?>

    <main role="main" class="container d-flex flex-column">
        <div class="card my-3 p-3 mt-5 mb-5 bg-white shadow-lg rounded w-75 align-self-center">
            <h1 class="border-bottom text-center font-weight-bold border-gray pb-2 mb-0">Notifiche Recenti</h1>
            <?php
            foreach ($_SESSION["notifications"] as $value) {
                $_SESSION["single_notification"] = $value;
                require "components/single_notification.php";
            }
            unset($_SESSION["single_notification"]);
            unset($_SESSION["notifications"]);
            ?>
        </div>
    </main>
    <div class="h-100">
    </div>
    <?php
    require "components/foot.php"
    ?>
    <script type="module">
        import * as notif from "../js/notifications.js";
        window.deleteNotification = notif.deleteNotification;
    </script>
</body>

</html>
<?php $_SESSION["notificationsPage"] = false; ?>