<?php require_once "../controllers/include.php"; ?>
<?php $category = $_GET["category"] ?>
<!DOCTYPE html>
<html lang="it">

<head>
    <?php require "components/head.php"; ?>
    <?php require "../controllers/catalog.php"; ?>
    <title>Asian Flavours - Catalogo prodotti</title>
</head>

<body>

    <?php
    require "components/navbar.php";
    ?>

    <main class="mt-5">

        <article class="mt-3">
            <div id="article-album" class="album py-3">
                <h2 class="text-center text-light font-weight-bold pl-3"><?php echo $category;?></h2>
                <div id="article-div" class="container-fluid d-flex justify-content-center">
                    <div id="article-card" class="row col-10 bg-light shadow-lg d-flex justify-content-center">
                        <?php
                            $cat = new Category($category);
                            foreach($cat->getProducts() as $prodID)
                            {
                                $prod = new Product($prodID);
                                $_SESSION["single_product"] = array(
                                    "name" => $prod->getName(),
                                    "description" => $prod->getDescription(),
                                    "id" => $prod->getID(),
                                    "price" => $prod->getPrice()
                                );
                                require "components/product_card.php";
                            }

                        ?>
                    </div>
                </div>
            </div>
        </article>
    </main>
    <?php
    require "components/foot.php"
    ?>
    <script src="../js/product_cart.js"></script>
</body>

</html>