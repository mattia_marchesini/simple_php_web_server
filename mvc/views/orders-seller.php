<div class="my-3 mb-5 p-3 bg-white rounded box-shadow shadow">
    <h2 class="border-0 text-center border-gray pb-2 mb-0">
        Ordini Ricevuti
    </h2>
    <?php $products = Seller::getMyOrders(); ?>
    <table id="my-products-table" class="table table-bordered shadow table-sm table-hover">
        <thead>
            <tr>
                <th scope="col">
                    #IDOrdine
                </th>
                <th scope="col">
                    Immagine
                </th>
                <th scope="col">
                    Nome
                </th>
                <th scope="col">
                    Quantità
                </th>
                <th scope="col">
                    Costo Totale
                </th>
                <th scope="col">
                    Data Ordine
                </th>
            </tr>
        </thead>
        <tbody>

            <?php
            foreach ($products as $product) :
            ?>
                <tr>
                    <?php
                    $idordine = $product["idordine"];
                    $data = $product["data"];
                    $dataordine = date('d/m/y', strtotime($data));
                    $nome = $product["nome"];
                    $quantità = $product["quantità"];
                    $prezzounitario = $product["prezzounitario"];
                    ?>
                    <td scope="row">
                        ID-<?php echo $idordine; ?>
                    </td>
                    <td scope="row">
                        <img class="img-fluid img-thumbnail h-5" src="../../resources/img/p<?php echo $product["idprodotto"]; ?>.jpg" alt="Ramen" height=100 width=100></img>
                    </td>
                    <td scope="row">
                        <?php echo $nome; ?>
                    </td>
                    <td>
                        <?php echo $quantità; ?>
                    </td>
                    <td>
                        <?php echo $prezzounitario * $quantità; ?> €
                    </td>
                    <td>
                        <?php echo $dataordine; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>