<html lang="it">

<head>
    <?php require 'components/head.php'; ?>
    <title>
        Inserisci Prodotto - Asian Flavours
    </title>
</head>

<body class="bg-gray">
    <!-- navbar -->
    <?php require 'components/navbar.php'; ?>
    <div id="page-content">
        <div class="container my-0 bg-gray">
            <div class="card d-flex rounded-10 shadow-lg justify-content-center mt-5 w-100">
                <div id="error-row" class="row mt-3 justify-content-center">
                    <span class="rounded px-1" id="error-span">
                        <small>
                            <?php
                            if (isset($_SESSION["err_insert"])) {
                                echo $_SESSION["err_insert"];
                                $_SESSION["err_insert"] = "";
                            }
                            ?>
                        </small>
                    </span>
                </div>
                <div id="insert-row" class="row justify-content-center">
                    <div id="insert-col" class="col-6">
                        <div id="insert-box">
                            <form method="post" action="../controllers/insert.php">
                                <div id="titolo-form" class="text-center">
                                    <h4>Inserisci prodotto</h4>
                                </div>
                                <div id="input-insert">
                                    <input type="text" class="form-control" placeholder="Nome Articolo" id="nomeprodotto" name="productname">
                                </div>
                                <div id="input-insert">
                                    <input type="text" class="form-control" placeholder="Descrizione" id="descrizione" name="description">
                                </div>
                                <div id="input-insert">
                                    <input type="number" min="1" class="form-control" placeholder="Quantità" id="quantità" name="quantity">
                                </div>
                                <div id="input-insert">
                                    <input type="number" min="0.1" step="0.01" class="form-control" placeholder="Prezzo Unitario" id="prezzo" name="price">
                                </div>
                                <div id="input-insert">
                                    <input type="number" min="0.1" step="0.1" class="form-control" placeholder="Peso Unitario" id="peso" name="weight">
                                </div>
                                <div id="input-insert">
                                    <input type="text" class="form-control" placeholder="Categoria" id="categoria" name="category">
                                </div>
                                <div id="submit-insert" class="text-center">
                                    <button id="submit" type="submit" class="btn-lg">Inserisci</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>