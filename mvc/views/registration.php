<?php
require_once("../controllers/include.php");
list($result, $seller) = $_SESSION["userhelper"]->checkFormData();
if ($result) {
    if ($seller) {
        Seller::registerNewSeller();
    } else {
        Customer::registerUser();
    }
    $_POST = array();
    $_SESSION["database"]->disconnectDB();
    header("Location: login_form.php");
    die();
} else {
    $_POST = array();
    $_SESSION["database"]->disconnectDB();
    if ($seller) {
        header("Location: registration-seller.php");
        die();
    } else {
        header("Location: registration-form.php");
        die();
    }
}
