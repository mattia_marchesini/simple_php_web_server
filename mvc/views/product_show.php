<!DOCTYPE html>
<html lang="it">

<head>
    <?php require "head.php" ?>
    <title>Asian Flavours</title>
</head>

<body>
    <?php require "navbar.php"; ?>
    <div class="container-fluid mt-5 d-flex justify-content-center">
        <div id="article-category" class="row w-75 h-100 shadow-lg rounded d-inline-flex justify-content-center">
            <div class="col-sm-5 col-md-4 col-lg-3 mx-auto h-100 d-flex justify-content-center">
                <?php
                $_SESSION["single_product"] = array(
                    "name" => "Ramen",
                    "description" => "ramen ramoso",
                    "id" => 1
                );
                require "product_card.php";
                ?>
            </div>
            <div class="position-relative col-sm-7 col-md-8 col-lg-7 pt-sm-4">
                <p class="lead fw-normal">a very loooong description full of details</p>
                <a class="btn btn-outline-secondary" href="#">Coming soon</a>
            </div>
        </div>
    </div>
    <script src="../js/product_card.js"></script>
</body>

</html>