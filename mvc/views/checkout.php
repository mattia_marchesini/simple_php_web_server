<html lang="it">

<head>
    <?php require 'components/head.php'; ?>
    <script type="text/javascript" src="../js/cart.js"></script>
    <title>
        Carrello - Asian Flavours
    </title>
</head>

<body class="bg-gray">
    <?php require 'components/navbar.php'; ?>
    <div class="h-25"></div>
    <div class="row">
        <div id="cart-card" class="col-lg-6 p-5 bg-white shadow-lg  mb-5">
            <span class="w-50">
                <h4 class="fs-3">Grazie per aver acquistato da noi!</h4>
            </span>
        </div>
    </div>
    <?php
    require "components/foot.php"
    ?>
    <script type="text/javascript" src="../js/footer.js"></script>
</body>