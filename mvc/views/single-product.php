<html lang="it">

<head>
    <?php require 'components/head.php'; ?>
    <title>
        Asian Flavours
    </title>
</head>

<body>
    <?php require "components/navbar.php"; ?>
    <?php $prodotto = new Product($_GET["id"]); ?>
    <div class="container-fluid mt-5 mb-5 d-flex justify-content-around align-items-center">
        <div style="background-color: salmon; max-width: 300px;" class="card row border d-flex justify-content-center shadow h-auto">
            <div class="inner-card col-lg-12 w-100">
                <img style="max-width: 300px" class="shadow mx-auto d-flex justify-content-center mx-5 w-100 bg-light mt-3 img-fluid rounded" alt="<?php echo $prodotto->getName(); ?>" src="../../resources/img/p<?php echo $prodotto->getId(); ?>.jpg">
                <div class="d-flex justify-content-center align-items-center mt-3 px-2">
                    <h4><?php echo $prodotto->getName(); ?></h4>
                </div>
                <div class="mt-2 text-center px-2">
                    <small>
                        <?php echo $prodotto->getDescription(); ?>
                    </small>
                </div>
                <div class="text-center px-2">
                    <?php echo $prodotto->getPrice(); ?>€
                </div>
                <div class="px-2 d-flex justify-content-center pb-3 mt-3">
                    <button class='btn px-3 border shadow mx-3 btn-prod-<?php echo $_GET["id"];?>'
                    onclick='addProduct(<?php echo $_GET["id"];?>)'>
                        aggiungi
                    </button>
                </div>
            </div>
        </div>
    </div>
    <?php
    require "components/foot.php"
    ?>
    <script src="../js/product_cart.js"></script>
</body>

</html>