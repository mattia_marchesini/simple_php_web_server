# Asian Flavours

Progetto per il corso di tecnologie web, anno 2020/2021, università di Bologna, sede di Cesena, facoltà di ingegneria e
scienze informatiche.

Autori: Mattia Marchesini, Lorenzo Massone.

La query per creare il database, riempirlo e il mock-up del sito si trovano nella cartella `resources`.

## Design

Il sito si ispira fortemente agli store online specializzati 
nella vendita di alimentari nella semplicità, nelle forme e
nei colori.

Dalla pagina `catalog.php` è da subito possibile aggiungere al carrello i prodotti in vendita divisi per categorie.

## Funzionamento

È possibile aggiungere elementi al carrello cliccando sul tasto "Aggiungi" di ogni singolo prodotto ed eliminarli con il tasto
"Elimina".
La navbar è lo strumento principale per muoversi all'interno delle varie sezioni del sito.
Una volta che la "spesa" è terminata è possibile andare al checkout cliccando sull'icona del carrello.

## Login e Registrazione

Per utilizzare in maniera efficace il sito è necessario che l'utente sia registrato. E' possibile registrarsi
tramite il form di registrazione diverso tra Clienti e Venditori per poi 
effettuare l'accesso tramite il form di login. Le password con cui l'utente si registra sono cifrate e passate 
tramite il metodo POST in modo da garantire una migliore sicurezza dei dati dell'utente.

## Carrello

Una volta arrivati al carrello è possibile eliminare i singoli prodotti o modificarne la quantità.
Per ogni prodotto è indicata la quantità presente nel carrello ( gestibile tramite appositi tasti) e il subtotale inerente allo specifico prodotto.
Al di sotto del carrello si trova il riassunto dell'ordine con costo totale ( con aggiunti IVA e spedizione).

## Notifiche

Una volta effettuato un ordine commparirà una notifica di spedizione in una
pagina dedicata accessibile tramite un'icona a campanella posta sulla navbar. Una volta visualizzate le notifiche esse
verranno contrassegnate come lette, azzerando il contatore delle nuove notifiche.

## Pagina utente

Nella pagina utente è possibile visualizzare i dati di registrazione dell'utente attualmente loggato e lo storico degli ordini da lui effettuati.
Qualora l'utente loggato sia un venditore, oltre alla lista degli ordini ricevuti, è possibile visualizzare la tabella dei suoi prodotti
attualmente inseriti nel sito.

Da qui il venditore può eventualmente diminuire o aumentare il numero di prodotti disbonibili, eliminarli o aggiungerne di nuovi.

