CREATE DATABASE asian_flavours;

CREATE TABLE clienti (
    idcliente INT PRIMARY KEY AUTO_INCREMENT,
    nome varchar(255) NOT NULL,
    cognome varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    città varchar(255) NOT NULL,
    indirizzo varchar(255) NOT NULL
);

CREATE TABLE venditori (
    idvenditore INT PRIMARY KEY AUTO_INCREMENT,
    nome varchar(255) NOT NULL,
    cognome varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    città varchar(255) NOT NULL,
    indirizzo varchar(255) NOT NULL,
    partitaIVA varchar(11) NOT NULL
);

CREATE TABLE ordini (
    idordine INT PRIMARY KEY AUTO_INCREMENT,
    data TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    idcliente INT NOT NULL,
    città varchar(255) NOT NULL,
    indirizzo varchar(255) NOT NULL,
    FOREIGN KEY (idcliente) REFERENCES clienti(idcliente)
);

CREATE TABLE prodotti (
    idprodotto INT PRIMARY KEY AUTO_INCREMENT,
    nome varchar(255) NOT NULL,
    descrizione varchar(255) NOT NULL,
    quantità INT UNSIGNED NOT NULL,
    prezzounitario INT UNSIGNED NOT NULL,
    pesounitario INT UNSIGNED NOT NULL,
    data TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    idvenditore INT NOT NULL,
    FOREIGN KEY (idvenditore) REFERENCES venditori(idvenditore)
);

CREATE TABLE prodotti_ordine (
    idprodotto INT NOT NULL,
    quantità INT UNSIGNED NOT NULL,
    idordine INT NOT NULL,
    FOREIGN KEY (idordine) REFERENCES ordini(idordine) ON DELETE CASCADE
);

CREATE TABLE prodotti_categorie(
    categoria varchar(255) NOT NULL,
    idprodotto INT NOT NULL,
    PRIMARY KEY (categoria, idprodotto)
);

CREATE TABLE notifiche_clienti (
    data TIMESTAMP NOT NULL,
    descrizione varchar(255) NOT NULL,
    letta BOOLEAN NOT NULL,
    idcliente INT NOT NULL,
    FOREIGN KEY (idcliente) REFERENCES clienti(idcliente) ON DELETE CASCADE,
    PRIMARY KEY (data, idcliente)
);

CREATE TABLE notifiche_venditori (
    data TIMESTAMP NOT NULL,
    descrizione varchar(255) NOT NULL,
    letta BOOLEAN NOT NULL,
    idvenditore INT NOT NULL,
    FOREIGN KEY (idvenditore) REFERENCES venditori(idvenditore) ON DELETE CASCADE,
    PRIMARY KEY (data, idvenditore)
);

CREATE TABLE prodotti_carrello(
    idprodotto INT,
    idcliente INT,
    quantità INT UNSIGNED NOT NULL,
    FOREIGN KEY (idprodotto) REFERENCES prodotti(idprodotto),
    FOREIGN KEY (idcliente) REFERENCES clienti(idcliente) ON DELETE CASCADE
);