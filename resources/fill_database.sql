USE asian_flavours;

INSERT INTO `venditori` (`idvenditore`, `nome`, `cognome`, `email`, `password`, `città`, `indirizzo`, `partitaIVA`) VALUES
(18, 'Ottavio', 'Armadietto', 'ottavioarmadietto@gmail.com', '$2y$10$fCEs843ejLeNXhQmj4cmGOufqHY3Lx8ZOdDMS13bMDExZEqVX/VNG', 'Torino', 'Via Venditori 12', '12345678907'),
(19, 'Mauro', 'Mau', 'mauromau@gmail.com', '$2y$10$vMNmt4DeK6YCbf6shbirWOhRhlQDM7hPB2GJj1Wm7K5AF7LBcEUBe', 'Mantova', 'Via dei Venditori 15', '11111111111'),
(20, 'Nicola', 'Simone', 'nicolasimone513@gmail.com', '$2y$10$yEGwHYPEw2VSimWfOdK4V.YHTnje0zCN.cACkPI0.b1UVMNi/OVXy', 'Vasto', 'Via Santa Lucia 40', '12333333333');

INSERT INTO `clienti` (`idcliente`, `nome`, `cognome`, `email`, `password`, `città`, `indirizzo`) VALUES
(1, 'Lorenzo', 'Massone', 'lorenzomassone@gmail.com', '$2y$10$woKfm1E2iwepxxufyook6.SbE4FScMQwW6Cbplv7AgcJ8LBH9d.f2', 'Cupello', 'Via Istonia'),
(2, 'Simone', 'Santo', 'simonesanto@gmail.com', '$2y$10$eUI8qDM9LeMeouC/wMhDYupwgTnz.tsPd/cut9oI6lNoBEaQLkFOO', 'Siracusa', 'Via dei Clienti 2');

INSERT INTO `notifiche_clienti` (`data`, `letta`, `descrizione`, `idcliente`) VALUES
('2021-02-09 16:46:26', 1, 'Notifica di prova!', 1);


INSERT INTO `prodotti` (`idprodotto`, `nome`, `descrizione`, `quantità`, `prezzounitario`, `pesounitario`, `data`, `idvenditore`) VALUES
(1, 'Yen Ramen', 'Ramen gusto Manzo e Verdura', 3, 2, 1, '2021-02-04 19:08:30', 19),
(2, 'Zen Ramen', 'Ramen gusto Pollo e Verdura', 3, 2, 1, '2021-02-04 19:08:30', 19),
(3, 'Ten Ramen', 'Ramen gusto Gamberi e Ostriche', 3, 2, 1, '2021-02-04 19:08:30', 19),
(5, '8 Ramen', 'Ramen Carciofo e Fragola', 16, 1, 1, '2021-02-07 15:20:58', 18),
(6, 'Dragon Ramen', 'Ramen condito con sangue di drago', 40, 5, 2, '2021-02-08 21:36:39', 19),
(7, 'Sichuan Ramen', 'Spicy Ramen alle verdure', 12, 2, 2, '2021-02-08 21:48:53', 19),
(9, 'Patatine al formaggio', 'Patatine croccanti al formaggio', 20, 2, 1, '2021-02-09 22:56:00', 19);

INSERT INTO `ordini` (`idordine`, `data`, `idcliente`, `città`, `indirizzo`) VALUES
(3, '2021-02-05 23:26:05', 1, 'Torino', 'Via Torinese 23'),
(7, '2021-02-06 20:32:25', 1, 'Lodi', 'Via Vai 31'),
(8, '2021-02-06 20:32:28', 1, 'Lodi', 'Via Vai 31');

INSERT INTO `prodotti_ordine` (`idprodotto`, `quantità`, `idordine`) VALUES
(1, 3, 8),
(2, 7, 8);

INSERT INTO `prodotti_carrello` (`idprodotto`, `idcliente`, `quantità`) VALUES
(3, 1, 6),
(5, 1, 5),
(6, 1, 7),
(7, 1, 5),
(1, 1, 1);

INSERT INTO `prodotti_categorie` (`categoria`, `idprodotto`) VALUES
('I migliori', 1),
('I migliori', 2),
('I migliori', 3),
('I migliori', 5),
('I migliori', 6),
('I migliori', 7),
('I migliori', 9),
('Nuovi arrivi', 1),
('Nuovi arrivi', 2),
('Nuovi arrivi', 3),
('Nuovi arrivi', 5),
('Nuovi arrivi', 6),
('Nuovi arrivi', 7),
('Più venduti', 1),
('Più venduti', 2),
('Più venduti', 3),
('Più venduti', 5),
('Più venduti', 6),
('Più venduti', 7);